package ga.ishan1608.reportpage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.DownloadListener;
import android.webkit.MimeTypeMap;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.webkit.JavascriptInterface;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private JsHandler jsHandler;

    private Map<String, String> headers = new HashMap<>();
    private String reportId = "ai71odqj3f";
    private WebView myWebView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WebView.setWebContentsDebuggingEnabled(true);

        headers.put("Authorization", "<api_key>");

        myWebView = (WebView) findViewById(R.id.webview);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

        jsHandler = new JsHandler(this, myWebView);
        myWebView.addJavascriptInterface(jsHandler, "JsHandler");

        myWebView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                jsHandler.jsDownloadUrl(url);
            }
        });

        myWebView.loadUrl("https://test.orgzit.com/app/report/" + reportId + "/", headers);
    }

    public class JsHandler {
        Activity activity;
        WebView webView;

        JsHandler(Activity context, WebView webView) {
            activity = context;
            this.webView = webView;
        }

        @JavascriptInterface
        public void data64Listener(String data64, String mimetype) {
            // Getting only the base64 encoded data, by removing the meta-data
            data64 = data64.split("base64,")[1];
            String extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(mimetype);


            try {
                // Saving the report to a temporary file
                File filesDir = activity.getFilesDir();
                File reportTempFile = new File(filesDir.getAbsolutePath()
                        + File.separator + "report-temp." + extension);
                FileOutputStream outputStream;
                outputStream = new FileOutputStream(reportTempFile);
                outputStream.write(Base64.decode(data64, Base64.DEFAULT));
                outputStream.close();

                // Get URI and MIME type of file
                String fileProvider = getPackageName() + ".fileprovider";
                Uri uri = FileProvider.getUriForFile(activity, fileProvider, reportTempFile);
                String mime = getContentResolver().getType(uri);

                // Open file with user selected app
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, mime);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void data64PrintListener(String data64) {
            Log.e(TAG, "data64PrintListener" + data64);
            // Getting only the base64 encoded data, by removing the meta-data
            data64 = data64.split("base64,")[1];
            byte[] decodedString = Base64.decode(data64, Base64.DEFAULT);
            Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            PrintHelper photoPrinter = new PrintHelper(activity);
            photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
            photoPrinter.printBitmap("report.png - print", decodedBitmap);
        }

        /**
        * This function handles call from Android-Java
        */
        void jsDownloadUrl(String url) {
            final String webUrl = "javascript:PMS.fn.downloadAMChartUrl('" + url + "')";

            // Add this to avoid android.view.windowmanager$badtokenexception unable to add window
            if(!activity.isFinishing()) {
                // loadurl on UI main thread
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        webView.loadUrl(webUrl);
                    }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            myWebView.loadUrl("https://test.orgzit.com/app/report/" + reportId + "/", headers);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
